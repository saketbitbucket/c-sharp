﻿using System;
using System.Collections.Generic;

namespace Polymorphism_and_Casting
{
    class MainProgram
    {
        public static void DrawAllShapes(List<Shape> shapes)
        {
            foreach (var s in shapes)
            {
                s.Draw();  // No changes in this function if new shape added this is polymorthism advantage

            }
        }

        /// <summary>
        /// Typecasting demo
        /// </summary>
        public static void DrawAllCircles(List<Shape> shapes)
        {
            Console.WriteLine("============Printing all circles===========");
            foreach (var item in shapes)
            {
                Circle c = item as Circle;

                if (c != null)
                    c.Draw();
            }
        }



        static void Main(string[] args)
        {
            Console.WriteLine("Static and Dynamic polymorphism, operator and function overloading");


            Console.WriteLine("Enter 1. circle 2. rect 3. square 4.Draw all shapes");
            int choice =0;

            List<Shape> shapes = new List<Shape>();
            do
            {
                choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case 1:  shapes.Add(new Circle());
                             break;

                    case 2:  shapes.Add(new Rectangle());
                             break;

                    case 3:  shapes.Add(new Square());
                             break;

                    case 4:  DrawAllShapes(shapes);
                             break;

                    default: break;
                }
            } while (choice != 4);


            DrawAllCircles(shapes);
        }
    }
}
