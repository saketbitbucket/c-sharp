﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Polymorphism_and_Casting
{
    class Shape
    {
        public virtual void Draw()
        {
            Console.WriteLine("Shape drawn -- basic implementation");
        }
    }

    class Circle : Shape
    {
        public override void Draw()
        {
            Console.WriteLine("Draw of circle !!!");
        }
    }

    class Rectangle : Shape 
    {
        public override void Draw()
        {
            Console.WriteLine("Draw rectangle !!!");
        }
    }

    class Square : Shape
    {
        public override void Draw()
        {
            Console.WriteLine("Draw Square !!!");
        }
    }


}
