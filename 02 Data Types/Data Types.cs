﻿using System;

namespace _02_Data_Types
{
    class Program
    {
        static void Main(string[] args)
        {
            // commonly used data type in C# are int, float, char, double, string and bool
            // Arrays is collection of data types

            int myintiger = 10;
            float myfloat = 20.0f;
            double mydouble = 20.0;
            char mychar = 'a';
            bool mybool = true;

            Console.WriteLine("my int is: "+ myintiger + " my float is: "+ myfloat + " my double is: "+ mydouble+ "mychar is "+mychar + " mybool is "+mybool);

            // Value type - represents an object or contains the type, if modifcation done to a value type it will effect only that variable
            // value type are of struct or enumeration
            // for value type assignment is deepcopy
            // Reference type - refers to an object or refers to a type,  if modification done to reference type it will effect all the
            // reference of that object.
            // For reference type assignment is shallow copy

            Console.WriteLine("-------TO DO : Nullable type in c#-----------");



            Console.WriteLine("-------Small calculator program-----------");

            Console.WriteLine("Enter first number  ");
            int num1 = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter second number ");
            int num2 = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Enter operator + - * / : ");
            char opr = Convert.ToChar(Console.Read());


            switch (opr)
            {

                case '+':
                    Console.WriteLine("output is " + (num1 + num2));
                    break;
                case '-':
                    Console.WriteLine("output is " + (num1 - num2));
                    break;
                case '*':
                    Console.WriteLine("output is " + (num1 * num2));
                    break;
                case '%':
                    Console.WriteLine("output is " + (num1 / num2));
                    break;

            }

        }
    }
}
