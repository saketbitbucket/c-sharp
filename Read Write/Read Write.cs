﻿using System;

namespace APP1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World! ");

            Console.Write("Enter your name : ");
            string name = Console.ReadLine();

            Console.Write("Enter your Age : ");
            int age = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter your height : ");
            double height = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("  " + name + "   " +  age + "   " + height);

            Console.WriteLine("**********Static triangle********");
            Console.WriteLine();
            Console.WriteLine("          /| ");
            Console.WriteLine("         / | ");
            Console.WriteLine("        /  | ");
            Console.WriteLine("       /___| ");

            Console.ReadLine();
        }
    }
}
