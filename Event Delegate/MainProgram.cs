﻿using System;

namespace Event_Delegate
{
    class MainProgram
    {
        static void Main(string[] args)
        {
            Console.WriteLine("-------------Event delegate example-----------------");

            EvenPublisher epublish = new EvenPublisher();
            EventSubscriber1 subscriber1 = new EventSubscriber1();
            EventSubscriber2 subscriber2 = new EventSubscriber2();

            epublish.SubscribeToEvent(subscriber1.EventHandler1);
            epublish.SubscribeToEvent(subscriber2.EventHandler2);

            epublish.DoSomeTask();

            Console.WriteLine("-------------Event delegate end-----------------");

        }
    }
}
