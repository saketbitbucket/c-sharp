﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Event_Delegate
{
    class EvenPublisher
    {
        public delegate void TaskDoneDelegate(object source, EventArgs arguments);

        public event TaskDoneDelegate TaskDoneEvent;

        public void DoSomeTask()
        {
            Thread.Sleep(1000);
            Console.WriteLine("Task done...");
            RaiseEvent();
        }

        public void SubscribeToEvent(TaskDoneDelegate del)
        {
            Console.WriteLine("Subscribed for event....");
            TaskDoneEvent += del;
        }

        public void UnsubscribeToEvent(TaskDoneDelegate del)
        {
            Console.WriteLine("Unsubscribed for event....");
            TaskDoneEvent = TaskDoneEvent - del;
        }

        public void RaiseEvent()
        {
            Console.WriteLine("Event Raised....");
            TaskDoneEvent(this, EventArgs.Empty);
        }
    }

    class EventSubscriber1
    {
        public void EventHandler1(object source, EventArgs args)
        {
            Console.WriteLine("Resived notification of event in observer 1....");
        }
    }

    class EventSubscriber2
    {
        public void EventHandler2(object source, EventArgs args)
        {
            Console.WriteLine("Resived notification of event in observer 2....");
        }
    }
}
