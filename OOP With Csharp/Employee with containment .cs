﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InheritanceWithCsharp
{
    class Date
    {
        private int day, month, year;

        public Date(int d, int m, int y)
        {
            day = d;
            month = m;
            year = y;
        }

        public int Day
        {
            get
            {
                return day;
            }
            set
            {
                day = value;
            }
        }

        public int Month
        {
            get
            {
                return month;
            }
            set 
            {
                month = value;
            }
        }
    
        public int Year
        {
            get 
            {
                return year;
            }
            set 
            {
                year = value;
            }
        }

        public void Display()
        {
            Console.Write("  " + day + "/" + month + "/" + year);
        }
    }


    class Employee
    {
        private char [] name;
        private int age;
        private Date dayofJoining; //containment

        public char[] Name
        {
            get 
            {
                return name;
            }

            set 
            {
                name = value; 
            }
        }

        public int Age
        {
            get 
            {
                return age;
            }

            set 
            {
                age = value;
            }
        }

        public Date JoiningDate
        {
            get 
            {
                return dayofJoining;
            }
            set 
            {
                dayofJoining = value;
            }
        }

        public Employee(char[] _name, int _age, Date Joining) //parameterise ctor
        {
            name = new char[_name.Length];
            
            name = _name;
            age = _age;
            dayofJoining = Joining;
        }
        public Employee(Employee other) //copy ctor
        {
            name = new char[other.name.Length];
            int i = 0;
            foreach (var item in other.name)
            {
                name[i++] = item;
            }

            age = other.age;
            dayofJoining = other.dayofJoining;
        }

        public void Display()
        {
            Console.Write("Employee Name: ");
            foreach (var item in name)
            {
                Console.Write(item);
            }
                
            Console.Write("  , Age: " + age);
            Console.Write("   Joing date ");
            dayofJoining.Display();
        }
    }
}
