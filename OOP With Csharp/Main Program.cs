﻿using System;

namespace InheritanceWithCsharp
{
    class OOP
    {
        static void Main(string[] args)
        {
            Console.WriteLine("--------------------------------------------Start-----------------------------------------------");
            Console.Write("Class , instance variable, property, get, set, protection level,static, const, inheritance\n\n\n");

            char[] name = { 'J', 'o', 'e' };
            Employee emp1 = new Employee(name, 30, new Date(02, 02, 2022));
            
            
            Employee emp2 = new Employee(emp1);
            emp2.Age = 33;
            emp2.Name[0] = 'D';

            emp1.Display();
            Console.WriteLine(" ");
            emp2.Display();

            Console.WriteLine("\n--------------------------------------------End-----------------------------------------------");
        }
    }
}
