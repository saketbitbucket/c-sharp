﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Event_Delegates
{
    class DelegatesDemo
    {
        // Creating two delegates
        // 1 . No return no parameter
        // 2.  Boool return and two int parameter
        
        //Difference between a method and a delegate is the keyword delegate
        public delegate void NoRetNoArgDelegate();
        public delegate bool BoolRetTwoArgDelegate(int num1, int num2);


        void func1()
        {
            Console.WriteLine("No arg no return function executing.....");
        }

        bool func2(int i, int j)
        {
            Console.WriteLine("int arg bool return function executing.....");
            return i > j;
        }


        public void InstanceAndExecuteNoArgDelegate()
        {
            NoRetNoArgDelegate delegate1 = func1 ;  // delgate instance delegate1 pointing to a function
            delegate1();
        }


        public void InstanceAndExecuteTwoArgDelegate(int arg1, int arg2)
        {
            BoolRetTwoArgDelegate delegate2 = new BoolRetTwoArgDelegate(func2); // delegate instance delegate2 pointing to func2
            delegate2(arg1, arg2);
        }

    }
}
