﻿using System;

namespace Event_Delegates
{
    class clientcode
    {
        public void FileLoadProgressed(int progress)
        {
            Console.WriteLine(progress + "% file loaded !!!");
        }

        public void UpdateDisplay(int progress)
        {
            Console.WriteLine("Update the progressbar to "+ progress + "% file loaded !!!");
        }
    }

    class MainPRogram
    {
        
        static void Main(string[] args)
        {
            Console.WriteLine("-----------------Basic program on delegates-----------------\n\n");

            DelegatesDemo obj = new DelegatesDemo();

            obj.InstanceAndExecuteNoArgDelegate();

            obj.InstanceAndExecuteTwoArgDelegate(10, 20);

            Console.WriteLine("-----------------Delegate for reporting progress-----------------\n\n");

            clientcode clentcd = new clientcode();
            FileReportprogress progress1 = new FileReportprogress();
            progress1.ProgressCallbackMethodDelegate = clentcd.FileLoadProgressed;
            progress1.ProgressCallbackMethodDelegate = clentcd.UpdateDisplay;
            progress1.LoadFile();
        }
    }
}
