﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Event_Delegates
{
    class FileReportprogress
    {
        public delegate void ProgressCallBack(int progressPercent);

        private ProgressCallBack progressDelegatel;

        public ProgressCallBack ProgressCallbackMethodDelegate
        {
            set 
            {
                progressDelegatel += value;
            }
        }

        private void ReportFileProgress(int progresspercent)
        {
            progressDelegatel(progresspercent);
        }

        public void LoadFile()
        {
            for (int i = 0; i < 10; i++)
            {
                Thread.Sleep(500);
                ReportFileProgress(i * 10);
            }
        }

    }
}
