﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Collections.Specialized;



namespace Collections
{
        class CollectionDemo
        {
                public static void CollectionDemoFunc()
                {
                    ////System.Collections classes
                    ////ArrayList
                    ///String collection
                    ////HashTable
                    ///

                    Console.WriteLine("1. Arraylist , 2.Stringcollection 3.Hashtable");
                    int choice = Convert.ToInt32(Console.ReadLine());

                    if (choice == 1)
                    {
                        ArrayList mixedItemList = new ArrayList() { 1, 2, 3, "Hi", "hello", 3.5f, 4.2 };

                        mixedItemList.Add("lastelement");

                        Console.WriteLine("Array list has : ");
                        foreach (object item in mixedItemList)
                        {
                            Console.Write("  " + item);
                        }

                        //find a data value in array list

                        Console.WriteLine("\n \nhello found in container - " + mixedItemList.Contains("hello"));

                    }

                    else if (choice == 2)
                    {
                        StringCollection strings = new StringCollection();
                        strings.Add("hi");
                        strings.Add("hii");
                        strings.Add("hiii");
                        strings.Add("hiiii");
                        strings.Add("hiiiii");

                        String[] arr = new string[] { "hello","helloo","hellooo","helloooo","hellooooo"};
                        strings.AddRange(arr);

                        Console.WriteLine("String collection has : ");
                        foreach (var item in strings)
                        {
                         Console.WriteLine(item);
                        }

                        //search
                        Console.WriteLine("String Collection has heloo - " + strings.Contains("heloo"));
                    }
                    else if(choice == 3)
                    {
                        Hashtable hashtbl = new Hashtable();
                        hashtbl.Add(1, "Hi");
                        hashtbl.Add("Hi", 1);

                        foreach (object item in hashtbl)
                        {
                            Console.WriteLine(item.ToString());
                        }

                        hashtbl.Contains("hi");
                    }

                }

        }
}
