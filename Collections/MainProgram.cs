﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Collections
{
    class MainProgram
    {
        static void Main(string[] args)
        {
            Console.WriteLine("1. System.collections , 2.System.collection.genrics");
            int choice = Convert.ToInt32(Console.ReadLine());

            if (choice == 1)
            {
                CollectionDemo.CollectionDemoFunc();
            }
            else if (choice == 2)
            {
                GenericCollectionDemo.GenericCollectionDemoFunc();
            }
        }
    }
}
