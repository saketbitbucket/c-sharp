﻿using System;
using System.Collections.Generic;

namespace Collections
{
    class GenericCollectionDemo
    {
        public static void GenericCollectionDemoFunc()
        {

            ////Initilization
            ////Add / remove
            ////Iterate
            ////Search

            ////System.Collections.Generic classes
            //List<T>
            //Dictionary<T>
            //HashSet<T>
            //LinkList<T>
            //Stack<T>
           
            Console.WriteLine("1. List<> , 2. Dictionary<> 3. HashSet<> ");
            int choice = Convert.ToInt32(Console.ReadLine());

            if (choice == 1)
            {
                Console.WriteLine("--------------------List Collection---------------------------");

                List<int> mylist = new List<int>() { 10, 100, 1000, 10000, 100000 }; //static initilization thru initilizer list

                for (int i = 0; i < 10; i++)
                {
                    mylist.Add(i + 1);
                }

                Console.WriteLine("Items in the list are : ");
                foreach (var item in mylist)
                {
                    Console.Write(" " + item);
                }


                List<int> newList = new List<int>(mylist);
                Console.WriteLine("\nItems in the new list are : ");
                foreach (var item in newList)
                {
                    Console.Write(" " + item * 10);
                }

                Console.WriteLine("\n\n Enter a number to search in the list using \"IndexOf\" method ");
                int key = Convert.ToInt32(Console.ReadLine());


                if (mylist.IndexOf(key) != -1)
                {
                    Console.WriteLine(key + " is found ");
                }
                else
                {
                    Console.WriteLine("not found");
                }


                Console.WriteLine("\n\n Enter a number to remove from list ");
                int rem = Convert.ToInt32(Console.ReadLine());
                if (!mylist.Remove(rem))
                {
                    Console.WriteLine("Failed to remove !!!");
                }
                else
                {
                    Console.WriteLine("New trimed list - ");
                    foreach (var item in mylist)
                    {
                        Console.Write(" " + item);
                    }
                }

            }

            else if (choice == 2)
            {
                Console.WriteLine("--------------------Dictionary in C#---------------------------");

                Dictionary<int, string> rollandNameDict = new Dictionary<int, string>() { { 1, "Joe" }, { 2, "Jonny" }, { 3, "Jhon" } };

                rollandNameDict.Add(4, "Jonathan");
                rollandNameDict[5] = "Jhoncena";

                Console.WriteLine("Students in class are !!! ");
                foreach (KeyValuePair<int, string> item in rollandNameDict)
                {
                    Console.WriteLine("Roll no " + item.Key + "  name " + item.Value);
                }

                Console.WriteLine("Enter the roll number to search - ");
                int roll = Convert.ToInt32(Console.ReadLine());

                if (rollandNameDict.ContainsKey(roll))
                {
                    var originalname = "";
                    rollandNameDict.TryGetValue(roll, out originalname);

                    Console.WriteLine("Enter new name ");
                    var newname = Convert.ToString(Console.ReadLine());
                    rollandNameDict[roll] = newname;
                    Console.WriteLine("Key " + roll + " had value " + originalname + " and is changed to " + rollandNameDict[roll]);
                }
                else
                {
                    Console.WriteLine("Name not found ");
                }
            }

            else if (choice == 3)
            {
                Console.WriteLine("--------------------Hash Set in C#--------------------------");

                HashSet<int> uniquesids = new HashSet<int>() { 1, 2, 3, 4, 4, 5, 1 };

                Console.WriteLine("Elements in set initialised with { 1, 2, 3, 4, 4, 5, 1 }");
                foreach (var item in uniquesids)
                {
                    Console.Write("  " + item);
                }

                Console.WriteLine("\n Enter values for the set  till -ve to stop");
                HashSet<int> uniquesnums = new HashSet<int>();
                var val = Convert.ToInt32(Console.ReadLine());

                while (val > 0)
                {
                    uniquesnums.Add(val);
                    val = Convert.ToInt32(Console.ReadLine());
                }
                Console.WriteLine("\n Elements in set initialised with ");
                foreach (var item in uniquesids)
                {
                    Console.Write("  " + item);
                }

                Console.WriteLine("\n 1. intersection , 2. union");
                val = Convert.ToInt32(Console.ReadLine());

                if (val == 2)
                {
                    uniquesids.UnionWith(uniquesnums);
                    Console.WriteLine("Union is  ");
                    foreach (var item in uniquesids)
                    {
                        Console.Write("  " + item);
                    }

                }
                else if (val == 1)
                {
                    uniquesids.IntersectWith(uniquesnums);
                    Console.WriteLine("Intersection is  ");
                    foreach (var item in uniquesids)
                    {
                        Console.Write("  " + item);
                    }
                }
            }
            

        }
    }
}
