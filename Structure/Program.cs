﻿using System;

namespace Structure
{
    class Program
    {

        public delegate int MySquareDelegate(int x);
        public delegate double MySquareDelegateDbl(double x);
        static void Main(string[] args)
        {
            Console.WriteLine("lambda expression");

            MySquareDelegate sq = x => x * x;  //this lambda expression no type required  => is call lambda operator

            MySquareDelegateDbl sqdbl = x => x * x;  //this lambda expression no type required  => is call lambda operator

            int retval1 = sq(5);
            Console.WriteLine("  MySquareDelegate sq = x => x * x  for sq(5) : " + retval1);

            double retval2 = sqdbl(2.3);
          
            Console.WriteLine("  MySquareDelegate sq = x => x * x  for sq(2.3) : " + retval2);
        }
    }
}
