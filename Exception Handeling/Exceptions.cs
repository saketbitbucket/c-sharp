﻿using System;

namespace Exception_Handeling
{
    
    class CustomException : Exception 
    {
        private string Name;
        public CustomException(string name)
        {
            Name = name;
        }

        public override string Message
        {
            get
            {
               return "Custom exception occured...";
            }
        }
    }

    class Exceptions
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Exception handeling ");


            try
            {
                int i = 0;
                int div = 500 / i;

            }
            catch (DivideByZeroException e)
            {
                Console.WriteLine(".........Caught exception..........");
                Console.WriteLine(e.StackTrace);
            }
            finally
            {
                Console.WriteLine("...............Doing final division.................");
            }


            Console.WriteLine("...............OWN exception class................");
            try
            {

                //do somthing 
                throw new CustomException("custom exception");
            }
            catch (CustomException E)
            {
                Console.WriteLine("caught exception " + E.Message);
            }
            finally
            {
                Console.WriteLine("Custom exception is handled !!!!");
            }
        }
    }
}
