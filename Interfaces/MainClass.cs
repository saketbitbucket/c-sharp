﻿using System;

namespace Interfaces
{
    class MainClass
    {
        static void Main(string[] args)
        {
            Console.WriteLine("\n----------------------------------------Concept------------------------------------------------------------");
            Console.WriteLine("Creating and using interfaces for circle class with shape interface");
            Console.WriteLine("two interfaces with same method can have explicit implementation i.e Interface1.func() interface2.func");
            Console.WriteLine("in this case func won't be accessible by class ref instead we have to use appropriate interface ref by casting (use as opertaor)");
            Console.WriteLine("class still can have a func without any interface name.. that will be called from the class ref");
            Console.WriteLine("\n----------------------------------------OUTPUT------------------------------------------------------------");

            Circle circle1 = new Circle();

            circle1.Draw();


            IDrawable draw1 = circle1 as IDrawable;

            if (draw1 != null)
            {
                draw1.Draw();
            }


            Ishape shape1 = circle1 as Ishape;

            if (shape1 != null)
            {
                shape1.Draw();
            }


            Console.WriteLine("\n-------------------------------------Interface with property------------------------------------------");

            Point randomPoint = new Point();
            randomPoint.X = 50;
            randomPoint.Y = 50;

            IPoint pt = randomPoint as IPoint;
            Console.WriteLine("Point " + randomPoint.X + "," + randomPoint.Y + "  distance from origine is : " + pt.DistanceFromOrigine());

            Console.WriteLine("\n-------------------------------------------END---------------------------------------------------------");

        }
    }
}
