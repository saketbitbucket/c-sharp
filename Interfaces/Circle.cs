﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    public class Circle : Ishape,IDrawable
    {
        void Ishape.Draw()
        {
            Console.WriteLine("Ishape Draw called for circle --------> Drawing the circle ");
        }

        void IDrawable.Draw()
        {
            Console.WriteLine("IDrawable Draw called for circle --------> Drawing the circle ");
        }

        public void Draw()
        {
            Console.WriteLine("Ishape and IDrawable Draw called for circle --------> Drawing the circle ");
        }
    }
}
