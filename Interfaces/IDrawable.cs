﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    public interface IDrawable
    {
        void Draw();
    }
}
