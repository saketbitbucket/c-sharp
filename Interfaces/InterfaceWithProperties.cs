﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    // Point interface with property for x and y coordinate
    interface IPoint
    {
        int X
        {
            get;
            set;
        }
        int Y
        {
            get;
            set;
        }

        double DistanceFromOrigine()  //Default function implementation
        {
           return Math.Sqrt( (X * X) + (Y * Y) );
        }
    }
}
