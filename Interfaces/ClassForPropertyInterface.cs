﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    class Point : IPoint
    {
        public int X 
        {
            get;
            set;
        }


        public int Y 
        { 
            get; 
            set; 
        }
    }
}
