﻿using System;

namespace Arrays
{
    class Arrays
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Arrays in C#");
            // In C#, arrays are collections of objects, and not just addressable regions of contiguous memory as in C and C++.
            // The default values of numeric array elements are set to zero, and reference elements are set to null.
            // Array is the abstract base type of all array types. You can use the properties and other class members
            // that Array has. An example of this is using the Length property to get the length of an array.

            // Array of value type(int,char,float,struct,enum) initializes to 0 vs Reference type(string, userClass)
            // Initilizes to null

            int[] arr = { 1, 2, 3, 4, 5 };


            int[] arr1 = new int[3];
            arr1[0] = 1;
            arr1[1] = 1;
            arr1[2] = 1;



            int size = 26;
            char[] alphabets = new char[size];
            for (int i = 0; i < size; i++)
            {
                alphabets[i] = Convert.ToChar('a' + i);
            }

            Console.WriteLine("Printing the chars using for each: ");
            foreach (var letter in alphabets)
            {
                Console.WriteLine(letter);
            }

            Console.WriteLine("------------- implicit Array :--->  var implicitArray = new[] { hi, hello }; ----------------");

            var implicitArray = new[] { "hi", "hello" };
            string[] messages = { "hi", "hello", "bye" };


            foreach (var message in messages)
            {
                Console.WriteLine(message);
            }

        }
    }
}
