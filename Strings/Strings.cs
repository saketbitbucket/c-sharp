﻿using System;

namespace Strings
{
    class Strings
    {
        static void Main(string[] args)
        {
            Console.WriteLine("------------------------Strings and StringBuilders-----------");

            string name = "joe";
            Console.WriteLine("name is " + name);

            name = "Doe"; // name now refer to a new object al together
            Console.WriteLine("name changed to  " + name);

            //change the value of strings chars not the string object
            string newname = name.Insert(0, "h");
            Console.WriteLine("old name " + name + "  new name is " + newname);

            const string message = "You can't assign new string object to this ref it is const";
            //message4 = "new msg"; // message is const and can't be assigned other string object

            //new operator with string class

            string dynamicstr = new string("hiii...");
            Console.WriteLine(dynamicstr);



            string s1 = "hello";
            string s2 = "world";

            s1 = s1 + s2;

            Console.WriteLine(" string s1 = hello , string s2 = world and concatination is  "+ s1);

            s1 = "hiii..";
            s2 = "hiii..";
            if (s1 == s2)
            {
                Console.WriteLine("Two strings are same ");
            }
            else 
            {
                Console.WriteLine("Two strings are different");
            }

            if(s1.Equals(s2))
            {
                Console.WriteLine("Two strings are same ");
            }
            else
            {
                Console.WriteLine("Two strings are different");
            }


            Console.WriteLine("String length is " + s1.Length);


            Console.WriteLine("Substring exists " + s1.Contains("i.."));
        }
    }
}
