﻿using System;
using System.Threading;


namespace Multi_Threading
{
    class Threading
    {
        static void RunFunction()
        {
            for (int i = 0; i < 10; i++)
            {
                Thread.Sleep(500);
                Console.WriteLine("Thread wth name \"" + Thread.CurrentThread.Name + "\" is executing !!");
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Thread library of c#");

            Console.WriteLine("Thread wth name \"" + Thread.CurrentThread.Name + "\" is executing !!");

            Thread newthread = new Thread(new ThreadStart(RunFunction));
            newthread.Name = "New Thread";
            newthread.Start();
            newthread.Join();
           
        }


    }
}
